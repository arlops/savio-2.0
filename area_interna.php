<?php 
// Template Name: Área Interna
?>

<?php get_header(); ?>

<section class="area-interna-section">
    <div class="savio-container">
        <div class='text-scroll'>
            <h2>
                Confira o conteúdo Interno Orçamentário da Cidade de Niterói que vai 
                facilitar o seu dia a dia administrativo
            </h2>
            <div class="button-flex">
                <a class="btn btn-primary-reverse" href="/planejamento-orcamentario">Planejamento</a>
                <a class="btn btn-primary-reverse" href="/category/execucao-oracamentaria/">Execução</a>
            </div>
        </div>
    </div>
</section>

<?php get_footer(); ?>