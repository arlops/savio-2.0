<?php 
// Template Name: Home
?>
<?php get_header(); ?>

<section class="home-section">

<div class="slide-container">
    <div class="slides" style="display: flex;">
            <div class="slide-text fade">
                <p>
                O sistema de autoatendimento virtual e interativo do orçamento é
                um conjunto de soluções para o orçamento público. Dentre elas,
                destacam-se a conta comercial de <span>whatsapp</span> com respostas
                automáticas a perguntas frequentes e dois
                repositórios de <span>conteúdo</span>, um externo e outro interno.   
                 </p>
                <div class="button-flex">
                    <a class="btn btn-primary" target="_blank" href="https://api.whatsapp.com/send?phone=5521980090993&text=Ol%C3%A1!">Whatsapp</a>
                    <a class="btn btn-primary" href="/conteudos">Conteúdo</a>
                </div>
            </div>
    </div>
    <div class="slides">
            <div class="slide-text fade">
                <p>
                Nosso objetivo é ser uma ferramenta para operadores do
                orçamento público. Cadastre-se e tenha acesso a documentos e
                conteúdos direcionados para o dia-a-dia do orçamento público.  
                </p>
                <a class="btn btn-primary" href="/login">Área Interna</a>
            </div>
    </div>

    <div class="slide-arrows">
        <div class="sidebar">
            <a class="next"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/next.svg"></a>
        </div>
        
        <div class="sidebar">
            <a class="prev"><img src="<?php echo get_template_directory_uri(); ?>/assets/img/back.svg"></a>
        </div>
    </div>
</div>

</section>



<?php get_footer(); ?>