<?php 
// Template Name: Conteudos
?>


<?php get_header(); ?>

<section class="apresentation-section">
    <div class="savio-container">
        <div class="conteudo-container">
            <div class="text-box">
                <h1>Conteúdo</h1>
                <p>
                Artigos sobre orçamento público
                </p>
                
                <?php get_search_form(); ?>
            </div>
            <video autoplay muted loop width="700" height="500">
                <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/conteudo.mp4" type="video/mp4">
            </video>
        </div>
    </div>
</section>

<section class="savio-section">
    <div class="savio-container artigos-page">
        <h1>Artigos</h2>
        <?php 
                
                $args = array(
                    'post_type' => 'conteudos',
                    'paged' => $currentPage
                );

                $query = new WP_Query($args);

                if( $query -> have_posts()) {
                    while ( $query -> have_posts()) {
                        $query -> the_post();                                        
                    
            ?>
            <div class="conteudo-card">
            <?php if(has_post_thumbnail()): ?>
                <a class="thumbnail-link" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
            <?php endif; ?>
                <div class="card-text">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <?php the_excerpt(); ?>
                    <span><?php the_date(); ?> - 
                    <?php 
                    $fname = get_the_author_meta('first_name');
                    $lname = get_the_author_meta('last_name');
                    $full_name = '';

                    if( empty($fname)){
                        echo $full_name = $lname;
                    } elseif( empty( $lname )){
                        echo $full_name = $fname;
                    } else {
                        //both first name and last name are present
                        echo $full_name = "{$fname} {$lname}";
                    }
                    ?>
                    (<?php the_author_meta('description'); ?>)
                    </span>
                    <a class="btn btn-sm btn-secondary" href="<?php the_permalink(); ?>">Ler mais</a>
                </div>
            </div>
        
            <?php
                }
            }
            ?>
    </div>
</section>

<?php get_footer(); ?>