<?php 
// Template Name: Execução Orçamentária
?>

<?php get_header(); ?>

<section class="savio-section">
<div class="savio-container">

    <div class="category-title">
        <img src="<?php echo get_template_directory_uri()?>/assets/img/consultant.svg">
        <h1><?php the_title(); ?></h1>
    </div>

    <div class="category-section">
    <?php 
    $categories = get_categories( array(
        'orderby' => 'name',
        'parent'  => 8
    ) );
     
    foreach ( $categories as $category ) {
        printf( '<a class="category-box-light" href="%1$s">%2$s</a><br />',
            esc_url( get_category_link( $category->term_id ) ),
            esc_html( $category->name )
        );
    }
    ?>
    </div>
</div>

</section>

<?php get_footer(); ?>