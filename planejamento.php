<?php 
// Template Name: Planejamento Orçamentária
?>

<?php get_header(); ?>

<section class="savio-section dark-mode">
<div class="savio-container">

    <div class="category-title">
        <img src="<?php echo get_template_directory_uri()?>/assets/img/budget.svg">
        <h1><?php the_title(); ?></h1>
    </div>

    <div class="category-section">
    <?php 
    $categories = get_categories( array(
        'orderby' => 'name',
        'parent'  => 14
    ) );
     
    foreach ( $categories as $category ) {
        printf( '<a class="category-box-dark" href="%1$s">%2$s</a><br />',
            esc_url( get_category_link( $category->term_id ) ),
            esc_html( $category->name )
        );
    }
    ?>
    </div>
</div>

</section>

<?php get_footer(); ?>