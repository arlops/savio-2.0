<?php 
// Template Name: Área Interna
?>

<?php get_header(); ?>

<section class="area-interna-section">
    <div class="savio-container">
        <div class='text-scroll'>
            <h2>
                Confira o conteúdo Interno Orçamentário da Cidade de Niterói que vai 
                facilitar o seu dia a dia administrativo
            </h2>
            <button id="scrollspy">
            <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                viewBox="0 0 213.333 213.333" style="enable-background:new 0 0 213.333 213.333;" xml:space="preserve">
            <g>
                <g>
                    <polygon points="0,53.333 106.667,160 213.333,53.333 		"/>
                </g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            <g>
            </g>
            </svg>
            </button>
        </div>
    </div>
</section>
        <a class="planejamento-btn-section" href="planejamento-orcamentario">Planejamento Orçamentário</a>
        <a class="execucao-btn-section" href="/category/execucao-oracamentaria/">Execução Orçamentário</a>

<?php get_footer(); ?>