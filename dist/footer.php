<?php wp_footer() ; ?>
    <?php 
        if (is_front_page()) {
            echo '<footer style="display:none;"></footer>';
        } else { ?>
        
        <footer class="savio-footer">
            <div class="savio-section footer-wrapper">
                <div class="savio-container">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Logo.png">
                    <div class="wrapper-col">
                        <div class="col">
                            <h3>Fale com o SAVIO</h3>
                            <a target="_blank" href="https://api.whatsapp.com/send?phone=5521980090993&text=Ol%C3%A1!">
                                    <svg width="67" height="67" viewBox="0 0 67 67" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M33.5084 0H33.4916C15.0206 0 0 15.0247 0 33.5C0 40.8281 2.36175 47.6203 6.37756 53.1352L2.20263 65.5804L15.0792 61.4641C20.3764 64.9733 26.6953 67 33.5084 67C51.9794 67 67 51.9711 67 33.5C67 15.0289 51.9794 0 33.5084 0ZM53.0012 47.3062C52.193 49.5884 48.9854 51.4811 46.4268 52.0339C44.6764 52.4066 42.3901 52.7039 34.6934 49.513C24.8486 45.4344 18.5088 35.4304 18.0146 34.7814C17.5414 34.1323 14.0365 29.4842 14.0365 24.6769C14.0365 19.8697 16.4778 17.5289 17.4619 16.5239C18.2701 15.6989 19.6059 15.3221 20.8872 15.3221C21.3018 15.3221 21.6745 15.343 22.0095 15.3597C22.9936 15.4016 23.4877 15.4603 24.1367 17.0138C24.9449 18.961 26.9131 23.7683 27.1476 24.2624C27.3862 24.7565 27.6249 25.4265 27.2899 26.0756C26.9759 26.7456 26.6995 27.0429 26.2054 27.6124C25.7113 28.1819 25.2422 28.6174 24.7481 29.2288C24.2959 29.7606 23.785 30.3301 24.3545 31.3141C24.924 32.2773 26.8921 35.4891 29.7899 38.0686C33.5293 41.3976 36.5611 42.4613 37.6456 42.9135C38.4538 43.2485 39.4169 43.1689 40.0074 42.5408C40.7569 41.7326 41.6824 40.3926 42.6246 39.0736C43.2946 38.1272 44.1404 38.0099 45.0282 38.3449C45.9327 38.659 50.719 41.0249 51.7031 41.5149C52.6871 42.009 53.3362 42.2435 53.5749 42.6581C53.8094 43.0726 53.8094 45.0198 53.0012 47.3062Z" fill="white"/>
                                    </svg>
                                    (21) 98009-0993   
                            </a>
                        </div>
                        <ul class="col">
                            <li><h3>Planejamento</h3></li>
                            <li><a href="/category/planejamento-orcamentario/lei-orcamentaria-anual/">Lei Orçamentária Anual</a></li>
                        </ul>
                        <ul class="col">
                            <li><h3>Execução</h3></li>
                            <li><a href="/modificacao-orcamentaria/">Modificação Orçamentária</a></li>
                            <li><a href="/ajuda-de-custo/">Ajuda de Custo</a></li>
                            <li><a href="/solicitacao-de-compra/">Solicitações de Compra</a></li>
                        </ul>
                        <ul class="col">
                            <li><h3>Área Externa</h3></li>
                            <li><a href="/sobre-nos">Sobre</a></li>
                            <li><a href="/conteudos">Conteúdo</a></li>
                        </ul>
                    </div>
                </div>
            </div>  
            <div class="footer-copyright">
                <p>Copyright © 2020 todos os direitos reservados | SAVIO</p>
            </div>  
        </footer>

    <?php
        }
    ?>

    </body>
</html>