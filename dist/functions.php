<?php

//adiciona scripts e estilos do tema
function add_styles_and_scripts() {
    wp_enqueue_style( 'style', get_template_directory_uri() . '/style.css');

    wp_enqueue_script('hamburger', get_template_directory_uri() . '/assets/js/hamburger.js');

    if (is_front_page()) {
        wp_enqueue_script('slide', get_template_directory_uri() . '/assets/js/slide.js');
    }
    
    if (is_page(230)) {
        wp_enqueue_script('scroll', get_template_directory_uri() . '/assets/js/scroll.js');
    }
    else {
        wp_enqueue_script('hamburger', get_template_directory_uri() . '/assets/js/hamburger.js');
    }
}

add_action('wp_enqueue_scripts', 'add_styles_and_scripts');



//adiciona edição de menus
function registrar_nav() {    
    register_nav_menu('header-menu', 'main-menu');
}

add_action('init', 'registrar_nav');


//reduzindo o tamanho dos resumos dos posts
function novo_tamanho_do_resumo($length) {
	return 20;
}
add_filter('excerpt_length', 'novo_tamanho_do_resumo');


//adiciona imagem às postagens
function suporte_a_thumbnails() {
    add_theme_support('post-thumbnails');
}

add_action('init', 'suporte_a_thumbnails');


//registrando conteudo externos

function registrar_conteudo() {

    $nomeSingular = 'Conteúdo';
    $nomePlural = 'Conteúdos';

    $labels = array(
        'name' => $nomePlural,
        'name_singular' => $nomeSingular,
        'add_new_item' => 'Adicionar novo ' . $nomeSingular,
        'edit_item' => 'Editar ' . $nomeSingular,
    );

    $supports = array(
        'title',
        'editor',
        'thumbnail'
    );

    $args = array(
        'labels' => $labels,
        'public' => true,
        'menu_icon' => 'dashicons-book',
        'supports' => $supports
    );

    register_post_type('conteudos', $args);
}

add_action('init', 'registrar_conteudo');

# registrando Taxonima à conteudo

$labels = array(
    'name' => 'Categorias',
    'nname_singular' => 'Categoria'
);

$args = array(
    'labels' => $labels,
    'public' => true,
    'hierarchical' => true
);

register_taxonomy('tipo_conteudo', 'conteudos', $args);

?>