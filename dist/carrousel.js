window.onload=function(){let menu=document.getElementById('navigation'),hamburger=document.getElementById('hamburger');function menu_drop(){if(menu.classList.length===1){menu.classList.add('open-menu')
hamburger.classList.add('cross-menu')}else if(menu.classList.length>1){menu.classList.remove('open-menu')
hamburger.classList.remove('cross-menu')}}
hamburger.addEventListener('click',menu_drop)
let next=document.getElementsByClassName('next'),prev=document.getElementsByClassName('prev'),inner=document.getElementsByClassName('inner'),slide=document.getElementsByClassName('slide'),currentImageIndex=0,width=900;function switchSlide(){inner[0].style.left=-width*currentImageIndex+'px';if(currentImageIndex===0){slide[0].getElementsByTagName('p').className='active'
slide[1].getElementsByTagName('p').className='deactive'}else{slide[0].getElementsByTagName('p').className='deactive'
slide[1].getElementsByTagName('p').className='active'}}
next[0].addEventListener('click',function(){currentImageIndex++;if(currentImageIndex>=slide.length){currentImageIndex=0}
switchSlide()});prev[0].addEventListener('click',function(){currentImageIndex--;if(currentImageIndex<0){currentImageIndex=slide.length-1}
switchSlide()});autoSlide();for(i=0;i<slide.length;i++){slide[i].onmouseover=pauseSlide;slide[i].onmouseout=resumeSlide};var interval=setInterval(autoSlide,7000);function pauseSlide(event){clearInterval(interval)}
function resumeSlide(event){interval=setInterval(autoSlide,7000)};function autoSlide(){interval;currentImageIndex++;if(currentImageIndex>=slide.length){currentImageIndex=0}
switchSlide()}}