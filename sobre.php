<?php 
// Template Name: Sobre
?>

<?php get_header(); ?>

<section class="apresentation-section">
    <div class="savio-container">
        <div class="gif-background">
            <div class="text-box">
                <h1>Solucionamos suas dúvidas            
                    sobre orçamento público</h1>
                <p>
                O SAVIO é um conjunto de ferramentas de gestão que nasceu das necessidades operacionais de execução do orçamento público. 
                Ele consiste em 3 funcionalidades a divulgação do conhecimento,
                a disponibilização de documentos, manuais e informações úteis 
                e o autoatendimento virtual por meio de uma conta comercial
                de Whatsapp com respostas
                automáticas.
                </p>
            </div>
            <video autoplay muted loop width="700" height="500">
                <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/Sobre.mp4" type="video/mp4">
            </video>
        </div>
    </div>
</section>

<section class="savio-section">
    <div class="savio-container flex-about">
        <div class="about-box">
            <h3>Objetivo</h3>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/objetivo.svg">
            <p>
            Oferecer soluções de comunicação e de gestão para o
            planejamento e a execução orçamentária no setor público
            </p>
        </div>
        <div class="about-box">
            <h3>Visão</h3>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/visao.svg">
            <p>
            Promover a eficácia, a eficiência e a efetividade da
            operacionalização do orçamento público.
            </p>
        </div>
        <div class="about-box">
            <h3>Missão</h3>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/missao.svg">
            <p>Ser uma referência de boas práticas no planejamento e na
            execução orçamentária no setor público
            </p>
        </div>
    </div>
</section>

<section class="transition">
        <div class="savio-container">
                <h1>
                    Saiba como o SAVIO vai
                    resolver os seus problemas
                </h1>
        </div>
</section>

<section class="tools-section">
    <div class="savio-ferramentas">
        <div class="ferramentas-container">
            <div class="tool-type conteudo">
                <h2>Conteúdo</h2>
                <p>
                O conteúdo do endereço eletrônico é composto por artigos elaborados por profissionais que
                atuam com o orçamento público. Nessa área do site é possível encontrar material teórico e
                prático desse campo de conhecimento evidenciando as principais referências sobre o tema,
                por meio de hiperlinks a outros sítios eletrônicos relacionados.
                </p>
            </div>
            
            <div class="tool-type area-interna">
                <h2>Área Interna</h2>
                <p>
                A área interna é uma parte do SAVIO sob medida ao operador do orçamento público. Nela é
                possível ter acesso a documentos, formulários e manuais específicos para atuação profissional.
                Para acessá-la é necessário que o usuário realiza o cadastro no SAVIO e faça o logon.
                </p>
            </div>
        </div>
    </div>
    <div class="savio-whatsapp">
        <div class="whatsapp-container">
            <div class="wpp-business">
                <h2>Fale com SAVIO</h2>
                <p>
                Entre em contato com a conta de Whatsapp comercial do SAVIO. Informe seu e-mail e siga as
                instruções para ter acesso às respostas automáticas. As perguntas são predeterminadas e
                foram desenvolvidas a partir das dúvidas mais frequentes em relação aos normativos de
                referência. As questões são organizadas por códigos numéricos, de modo que as centenas e os
                milhares correspondem ao assunto e os demais números referem-se às perguntas em si.
                </p>
            </div>
            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/celular.png" />
        </div>
    </div>
</section>


<?php get_footer(); ?>