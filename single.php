<?php get_header(); ?>




<section class="savio-section">
    <div class="savio-container-fluid d-flex">
        
        <?php 
            if (have_posts()) {
                while (have_posts()) {
                    the_post();
            ?>
                <div class="show-content">
                    <h1><?php the_title(); ?></h1>
                    <h3>Publicado em <?php the_date(); ?></h3>
                    
                        <?php 
                            $link = get_field('nome_do_autor');
                            if( $link ): 
                                $link_url = $link['url'];
                                $link_title = $link['title'];
                                $link_target = $link['target'] ? $link['target'] : '_self';
                        ?>
                        <h3>Autor - 
                            <a class="author-link" href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a>
                        </h3>
                        <?php endif; ?>
                    <p><?php the_content(); ?></p>
                </div>
                
            <?php
                }
            }
            ?>
            <div class="post-sidebar">
                    <?php if(is_active_sidebar('savio_sidebar')): ?>
                        <?php dynamic_sidebar('savio_sidebar'); ?>
                    <?php endif; ?>
                    <div class="sidebar-content">
                    <?php
                        $file1 = get_field('arquivo_de_download_1');
                        $file2 = get_field('arquivo_de_download_2');
                        $file3 = get_field('arquivo_de_download_3');
                        $file4 = get_field('arquivo_de_download_4');
                        if( $file1 || $file2 || $file3 || $file4 ): ?>
                            <h2>Download do arquivo</h2>
                            <div class="download-area">
                                <?php if($file1): ?>
                                <a class='sidebar-download' href="<?php echo $file1['url']; ?>">
                                    <div class="icon-download">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/file.svg">
                                    </div>
                                    <p><?php echo $file1['title']; ?></p>
                                </a>
                                <?php endif; 
                                    if ($file2): 
                                ?>
                                <a class='sidebar-download' href="<?php echo $file2['url']; ?>">
                                    <div class="icon-download">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/file.svg">
                                    </div>
                                    <p><?php echo $file2['title']; ?></p>
                                </a>
                                <?php endif; 
                                    if ($file3): 
                                ?>
                                <a class='sidebar-download' href="<?php echo $file3['url']; ?>">
                                    <div class="icon-download">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/file.svg">
                                    </div>
                                    <p><?php echo $file3['title']; ?></p>
                                </a>
                                <?php endif; 
                                    if ($file4): 
                                ?>
                                <a class='sidebar-download' href="<?php echo $file4['url']; ?>">
                                    <div class="icon-download">
                                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/file.svg">
                                    </div>
                                    <p><?php echo $file4['title']; ?></p>
                                </a>
                                <?php endif; ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="sidebar-content">
                        <?php 
                            if(has_tag()):
                        ?>
                            <h2>Palavras-chave</h2>
                            <div class="sidebar-tags">
                            <?php the_tags('', '', ''); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                    <div class="sidebar-content">
                        <h2>Últimas Publicações</h2>
                        <div class="sidebar-news">
                            <?php
                            $args = array( 'numberposts' => 3, 'order'=> 'DES', 'orderby' => 'date' );
                            $postslist = get_posts( $args );
                            foreach ($postslist as $post) :  setup_postdata($post); ?> 
                                <a href="<?php the_permalink(); ?>" class="last-post-box">
                                    <?php the_post_thumbnail(); ?>
                                    <div>
                                        <?php the_title(); ?>
                                        <p>
                                            <?php 
                                            $fname = get_the_author_meta('first_name');
                                            $lname = get_the_author_meta('last_name');
                                            $full_name = '';

                                            if( empty($fname)){
                                                echo $full_name = $lname;
                                            } elseif( empty( $lname )){
                                                echo $full_name = $fname;
                                            } else {
                                                //both first name and last name are present
                                                echo $full_name = "{$fname} {$lname}";
                                            }
                                            ?>    
                                        </p>
                                        <span><?php the_time('d/n/Y'); ?></span>
                                    </div>
                                </a>      
                            <?php endforeach; ?>
                        </div>
                    </div>
                    
                </div>
    </div>
</section>

<?php get_footer(); ?>