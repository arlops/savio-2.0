<?php get_header(); ?>

<section class="savio-section">
<div class="savio-container">
<?php 
    if (have_posts()) {
        while (have_posts()) {
            the_post();
    ?>
            <h1 class="index-title"><?php the_title(); ?></h1>
            <?php the_content(); ?>
    
    <?php
        }
    }
    ?>
</div>

</section>

<?php get_footer(); ?>