<?php
// Template Name: Confirmacao de Conta
?>

<?php get_header(); ?>


<?php 
    $image = get_field('imagem_fundo');
    if( !empty( $image ) ):
    ?> 
        <section class="dark-mode" style="height: 70vh; padding: 70px 0; background-size: cover; background-image: url(<?php echo $image['url']; ?>)">     
    <?php endif; ?>
    <div class="savio-container">
        <h1><?php the_field('titulo_principal'); ?></h1>
        <h3><?php the_field('mensagem'); ?></h3>
    </div>
</section>

<?php get_footer(); ?>