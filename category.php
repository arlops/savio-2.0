<?php get_header(); ?>

<section class="apresentation-section">
    <div class="savio-container">
        <div class="conteudo-interno-container">
        <?php 
                $category = get_queried_object();
                if ($category->term_id === 16) {

                    ?>
                    <video autoplay muted loop width="700" height="500">
                        <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/lei_anual.mp4" type="video/mp4">
                    </video>
            <?php 
                } else if ($category->term_id === 9){

                ?>
                <video autoplay muted loop width="700" height="500">
                    <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/adiantamento.mp4" type="video/mp4">
                </video>
            <?php 
                } else if ($category->term_id === 15){

                ?>
                <video autoplay muted loop width="700" height="500">
                    <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/lei_diretrizes.mp4" type="video/mp4">
                </video>
            <?php 
                } else if ($category->term_id === 17){

                ?>
                <video autoplay muted loop width="700" height="500">
                    <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/plano_plurianual.mp4" type="video/mp4">
                </video>
            <?php 
                } else if ($category->term_id === 10){

                ?>
                <video autoplay muted loop width="700" height="500">
                    <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/ajuda_custos.mp4" type="video/mp4">
                </video>
            <?php 
                } else if ($category->term_id === 11){

                ?>
                <video autoplay muted loop width="700" height="500">
                    <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/sol_compras.mp4" type="video/mp4">
                </video>
            <?php 
                } else if ($category->term_id === 12){

                ?>
                <video autoplay muted loop width="700" height="500">
                    <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/lib_empenho.mp4" type="video/mp4">
                </video>
            <?php 
                } else if ($category->term_id === 13){

                ?>
                <video autoplay muted loop width="700" height="500">
                    <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/lib_modific.mp4" type="video/mp4">
                </video>
            <?php
            } else {
            ?>
            <video autoplay muted loop width="700" height="500">
                <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/lib_modific.mp4" type="video/mp4">
            </video>
            <?php
                }
            ?>
            <div class="intro-box">     
                <h1><?php single_tag_title(); ?></h1>
                <?php get_search_form(); ?>
            </div>
        </div>
    </div>
</section>

<section class="savio-section">
    <div class="savio-container artigos-page">
        <?php 

            if(have_posts()) {
                while (have_posts()) {
                   the_post();                                        
                    
            ?>
            <div class="conteudo-card">
            <?php if(has_post_thumbnail()): ?>
                <a class="thumbnail-link" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
            <?php endif; ?>
                <div class="card-text">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <?php the_excerpt(); ?>
                    <span><?php the_date(); ?> - 
                    <?php 
                    $fname = get_the_author_meta('first_name');
                    $lname = get_the_author_meta('last_name');
                    $full_name = '';

                    if( empty($fname)){
                        echo $full_name = $lname;
                    } elseif( empty( $lname )){
                        echo $full_name = $fname;
                    } else {
                        //both first name and last name are present
                        echo $full_name = "{$fname} {$lname}";
                    }
                    ?>
                    </span>
                    <a class="btn btn-sm btn-secondary" href="<?php the_permalink(); ?>">Ler mais</a>
                </div>
            </div>
        
            <?php
                }
            }
            ?>
    </div>
</section>

<?php get_footer(); ?>