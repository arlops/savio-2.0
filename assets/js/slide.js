window.onload = function() {
    let menu_item = document.getElementsByClassName('menu-item');

    menu_item.forEach(function(menu) {
        if(menu.childNodes.length > 1) {
            menu.addEventListener('mouseover', function() {
                menu.childNodes[2].style.display = 'block'
            })
            menu.addEventListener('mouseout', function() {
                menu.childNodes[2].style.display = 'none'
            })
        }
    })
    
    let menu = document.getElementById('navigation'),
    hamburger = document.getElementById('hamburger');

    function menu_drop() {
        if (menu.classList.length === 1) {
            menu.classList.add('open-menu')
            hamburger.classList.add('cross-menu')
        }  else if(menu.classList.length > 1) {
            menu.classList.remove('open-menu')
            hamburger.classList.remove('cross-menu')
        }
    }
    
    hamburger.addEventListener('click', menu_drop);
    

    /* SLIDE CARROUSEL */

    let m = 1;
    let slides = document.querySelectorAll('.slides');
    let itemCount = slides.length;
    let slides_content = document.querySelectorAll('.slide-text')

    function slideShow(n){
        let targetSlide = slides[n-1];

        slides.forEach(function(slide) {
            if (slide != targetSlide) {
                slide.style.display = 'none'
            }
        })

        targetSlide.style.display = 'flex'
    }

    let interval = false;

    if (!interval) {
        interval = setInterval(function(){
            m = m + 1;
            if ( m > itemCount) { m = 1}
            slideShow(m);
        },5000);
    }



    for (i = 0; i<itemCount; i++){
        slides_content[i].addEventListener('mouseover', function() {
            clearInterval(interval);
        })
        slides_content[i].addEventListener('mouseout', function() {
            interval = setInterval(function(){
                m = m + 1;
                if ( m > itemCount) { m = 1}
                slideShow(m);
            },5000);
        })
    };

    let prev = document.getElementsByClassName('prev')[0];
    let next = document.getElementsByClassName('next')[0];

    prev.addEventListener('click', function() {
        m = m - 1;
        if ( m < 1 ) { 
            m = itemCount 
        }
        slideShow(m);
    })

    next.addEventListener('click', function() {
        m = m + 1;
        if ( m > itemCount ) { 
            m = 1 
        }
        slideShow(m);
    })
}