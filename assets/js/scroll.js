window.onload = function() {

    let menu = document.getElementById('navigation'),
    hamburger = document.getElementById('hamburger');

    function menu_drop() {
        if (menu.classList.length === 1) {
            menu.classList.add('open-menu')
            hamburger.classList.add('cross-menu')
        }  else if(menu.classList.length > 1) {
            menu.classList.remove('open-menu')
            hamburger.classList.remove('cross-menu')
        }
    }
    
    hamburger.addEventListener('click', menu_drop);
    
    let btn = document.getElementById('scrollspy');

    function scroll() {
        window.scrollTo({
            top: 720,
            left: 0,
            behavior: 'smooth'
          });
    }

    btn.addEventListener('click', scroll);

    let menu_item = document.getElementsByClassName('menu-item');

    menu_item.forEach(function(menu) {
        if(menu.childNodes.length > 1) {
            menu.addEventListener('mouseover', function() {
                menu.childNodes[2].style.display = 'block'
            })
            menu.addEventListener('mouseout', function() {
                menu.childNodes[2].style.display = 'none'
            })
        }
    })
}