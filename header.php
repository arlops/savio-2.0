
<!doctype html>
<html lang="pt-br">

<head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width">
    <title>
        <?php 
        bloginfo('name');
        if( !is_front_page() ) {
            echo ' | ' ;
            the_title(); 
        } 
        ?>
    </title>
    <?php wp_head(); ?>
</head>

<body>
        <?php 
            if (is_user_logged_in()) {
            ?>          
            <header class="savio-header">
                <div class="savio-container flex-section">

                <div class="savio-logo">
                    <a href="/area-interna">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Logo.png">
                    </a>
                </div>
                <button id="hamburger" class="hamburger" type="button" >
                    <span></span>
                    <span></span>
                    <span></span>
                </button>

                <?php 

                    $args = array(
                        'theme-location' => 'header-menu',
                        'container_class' => 'menu-nav',
                        'container_id'    => 'navigation',
                    );
                
                    wp_nav_menu($args);
                
                ?>
                </div>
                </div>
            </header>
        <?php
            } else {
            ?>

            <header class="savio-header">
                <div class="savio-container flex-section">

                <div class="savio-logo">
                    <a href="/">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/img/Logo.png">
                    </a>
                </div>

                <button id="hamburger" class="hamburger" type="button" >
                    <span></span>
                    <span></span>
                    <span></span>
                </button>

                <?php 

                    $args = array(
                        'theme-location' => 'header-menu',
                        'container_class' => 'menu-nav',
                        'container_id'    => 'navigation',
                    );
                
                    wp_nav_menu($args);
                
                ?>
                </div>
            </header>

        <?php
            }
        ?>