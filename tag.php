<?php get_header(); ?>

<section class="apresentation-section">
    <div class="savio-container">
        <div class="conteudo-interno-container">
            <video autoplay muted loop width="700" height="500">
                <source src="<?php echo get_template_directory_uri(); ?>/assets/videos/adiantamento.mp4" type="video/mp4">
            </video>
            <div class="intro-box">     
                <h1><?php single_tag_title(); ?></h1>
                <?php get_search_form(); ?>
            </div>
        </div>
    </div>
</section>

<section class="savio-section">
    <div class="savio-container artigos-page">
        <?php 

            if(have_posts()) {
                while (have_posts()) {
                   the_post();                                        
                    
            ?>
            <div class="conteudo-card">
            <?php if(has_post_thumbnail()): ?>
                <a class="thumbnail-link" href="<?php the_permalink(); ?>"><?php the_post_thumbnail(); ?></a>
            <?php endif; ?>
                <div class="card-text">
                    <h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                    <?php the_excerpt(); ?>
                    <span><?php the_date(); ?> - 
                    <?php 
                    $fname = get_the_author_meta('first_name');
                    $lname = get_the_author_meta('last_name');
                    $full_name = '';

                    if( empty($fname)){
                        echo $full_name = $lname;
                    } elseif( empty( $lname )){
                        echo $full_name = $fname;
                    } else {
                        //both first name and last name are present
                        echo $full_name = "{$fname} {$lname}";
                    }
                    ?>
                    (<?php the_author_meta('description'); ?>)
                    </span>
                    <a class="btn btn-sm btn-secondary" href="<?php the_permalink(); ?>">Ler mais</a>
                </div>
            </div>
        
            <?php
                }
            }
            ?>
    </div>
</section>

<?php get_footer(); ?>